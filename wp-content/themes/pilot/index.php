<?php get_header(); ?>
	<aside class="sidebar">
		<?php wp_list_categories(); ?>
    <!--ul class="categories">
			<?php while (have_posts()): the_post();
			if ($post->post_name == $slug) $active = "current-cat";
			else $active = ""; ?>
			<li class="<?php echo $active; ?>">
				<a href="<?php echo get_the_permalink()?>">
					<?php echo get_the_title()?>
				</a>
			</li>
			<?php endwhile ?>
		</ul -->

		<a href="https://www.youtube.com/watch?v=z2pYChv_T7Q" class="magnific pilot-video-button">Watch "I Love My Gym"</a>
	</aside>

	<div class="archive-content">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );
					//the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'views/content', 'search' ); ?>
			<?php endwhile; ?>
			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'views/content', 'none' ); ?>

		<?php endif; ?>
	</div>
<?php get_footer(); ?>