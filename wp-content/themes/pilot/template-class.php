<?php

/**
 * Created by PhpStorm.
 * User: SkyNet
 * Date: 2/10/15
 * Time: 12:05 AM
 */

$query = new WP_Query( array(
	'post_type' => 'wcs3_class',
	'posts_per_page' => -1));

// get all locations
$loc_query = new WP_Query( array(
	'post_type' => 'wcs3_location',
	'posts_per_page' => -1));

global $post;
$slug = get_post( $post )->post_name; 
 get_header();
 ?>

<div class="container">
	<aside class="sidebar">
	       
    <ul class="categories">

			<?php while ($query -> have_posts()): $query->the_post();

			if ($post->post_name == $slug) $active = "current-cat";

			else $active = ""; ?>

			<li class="<?php echo $active; ?>">

				<a href="<?php echo get_the_permalink()?>">

					<?php echo get_the_title()?>

				</a>

			</li>

			<?php endwhile ?>
		</ul>

		<a href="https://www.youtube.com/watch?v=z2pYChv_T7Q" class="magnific pilot-video-button">Watch "I Love My Gym"</a>

	</aside>

	<div class="class-content">

		<?php while ( have_posts() ) : the_post(); $class = get_the_title(); ?>

			<article <?php post_class(); ?>>

				<div class="entry-content">
					<header>
						<h2 class='entry-title'><?php echo get_the_title(); ?></h2>
					</header>
					<?php the_content(); ?>

					<?php if( 'wcs3_class' == $post->post_type ) : // don't query if main Classes page?>

						<?php while ($loc_query -> have_posts()): $loc_query->the_post(); // iterate over each location ?>

						<h3><?php echo get_the_title(); ?></h3>

						<div class='locations-calendar'>
							<div class='gradient'></div>
								<?php

									$atts = array(
										'layout'     => 'normal',
										'class'      => $class,
										'instructor' => 'all',
										'style'      => 'normal',
										'location'   => get_the_title()
									);

									echo wcs3_standard_shortcode( $atts );
								?>
						</div>

					<?php endwhile; wp_reset_postdata(); endif; ?>

				</div>

				<div class="footlinks">

					<?php wp_link_pages( array(

						'before' => '<nav class="page-nav"><p>' . __( 'Pages:', 'roots' ),

						'after'  => '</p></nav>'

					) ); ?>

				</div>

				<?php //comments_template( '/templates/comments.php' ); ?>

			</article>

		<?php endwhile; ?>

	</div>
</div>

<?php get_footer(); ?>

