<?php
/**
 * Created by PhpStorm.
 * User: SkyNet
 * Date: 2/10/15
 * Time: 12:05 AM
 */

$query = new WP_Query( array(
	'post_type' => 'wcs3_location',
	'posts_per_page' => -1));

$class_query = new WP_Query( array(
	'post_type' => 'wcs3_class',
	'posts_per_page' => -1));

global $post;
$slug = get_post( $post )->post_name; 
get_header();
?>

<div class="container">
	<aside class="sidebar">
		<ul class="categories">
			<?php
			$active = "";
			while ($query -> have_posts()): $query->the_post();
			if ($post->post_name == $slug) $active = "current-cat";
			else $active = ""; ?>

				<li class="<?php echo $active; ?>">
					<a href="<?php echo get_the_permalink()?>">
						<?php echo get_the_title(); ?>
					</a>
				</li>
			<?php endwhile ?>
       <li><a href="http://www.crossfit8.com/#welcome" target="_blank">Crossfit 8 Count</a></li>
		</ul>

		<a href="https://www.youtube.com/watch?v=z2pYChv_T7Q" class="magnific pilot-video-button">Watch "I Love My Gym"</a>		
	</aside>

	<div class="locations-content">
		<?php while ( have_posts() ) : the_post(); ?>
		<?php $location = get_the_title(); ?>
			<article <?php post_class(); ?>>
				<div class="entry-content">
					<h2 class="entry-title"><?php echo get_the_title(); ?></h2>
					<?php the_content(); ?>
					<div class="locations-calendar">
						<h3 class="entry-title"><?php echo get_the_title(); ?></h3>
						<hr/>

						<div class='locations-calendar'>
							<div class='gradient'></div>
								<?php

									$atts = array(
										'layout'     => 'normal',
										'class'      => 'all',
										'instructor' => 'all',
										'style'      => 'normal',
										'location'   => get_the_title()
									);

									echo wcs3_standard_shortcode( $atts );
								?>
						</div>

					</div>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</div>

<?php get_footer(); ?>
