<?php 

add_post_type_support( 'competitors', 'thumbnail' ); 
function create_competitors() {
  register_post_type( 'competitors',
    array(
      'labels' => array(
        'name' => __( 'Competitors' ),
        'singular_name' => __( 'Competitor' ),
        'view_items' => 'View Competitors',
      ),
      'public' => true,
      'rewrite' => array('slug' => 'competitors'),
    )
  );
}
add_action( 'init', 'create_competitors' );

?>