<?php
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	function acf_load_cdn_field_choices( $field ) {
	    $field['choices'] = array();
		$type = get_field('theme_type', 'option');
		$gender = get_field('theme_gender', 'option');
		$url = 'http://fitmaster.wpengine.com/wp-json/wp/v2/media?filter[media_category]='.$type.'%2B'.$gender;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		$media = json_decode($output);
		foreach( $media as $media ){
			$url = $media->source_url;
			$field['choices'][$url] = "<img width='100px' src='".$url."'>";
		}
		curl_close($ch);    
		return $field;   
	}
	add_filter('acf/load_field/name=cdn_image', 'acf_load_cdn_field_choices');
	
	function asset_path($filename) {
		$dist_path = get_template_directory_uri() . DIST_DIR;
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;
		
		if (empty($manifest)) {
			$manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get().array($file);
		} else {
			return $dist_path . $directory . $file;
		}
	}

	function get_custom_post_type_template($single_template) {
	    global $post;

	    if ($post->post_type == 'wcs3_class') {
	        $single_template =  get_template_directory() .'/template-class.php';
	    }
	    if ($post->post_type == 'wcs3_location') {
	        $single_template =  get_template_directory() .'/template-location.php';
	    }
	    return $single_template;
	}
	add_filter( 'single_template', 'get_custom_post_type_template' );

	add_action( 'init', 'create_class_taxonomies' );
	function create_class_taxonomies() {
	    // Add new taxonomy, make it hierarchical (like categories)
	    $labels = array(
	        'name'              => _x( 'Class Type', 'taxonomy general name' ),
	        'singular_name'     => _x( 'Class Type', 'taxonomy singular name' ),
	        'search_items'      => __( 'Search Class Types' ),
	        'all_items'         => __( 'All Class Types' ),
	        'parent_item'       => __( 'Parent Class Type' ),
	        'parent_item_colon' => __( 'Parent Class Type:' ),
	        'edit_item'         => __( 'Edit Class Type' ),
	        'update_item'       => __( 'Update Class Type' ),
	        'add_new_item'      => __( 'Add New Class Type' ),
	        'new_item_name'     => __( 'New Class Type Name' ),
	        'menu_name'         => __( 'Class Type' ),
	    );

	    $args = array(
	        'hierarchical'      => true,
	        'labels'            => $labels,
	        'show_ui'           => true,
	        'show_admin_column' => true,
	        'query_var'         => true,
	        'rewrite'           => array( 'slug' => 'class-type' ),
	    );

	    register_taxonomy( 'class-type', array( 'wcs3_class' ), $args );
	    add_post_type_support( 'wcs3_instructor', 'thumbnail' ); 
	}

?>