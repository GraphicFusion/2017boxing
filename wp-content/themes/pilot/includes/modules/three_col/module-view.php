<?php
global $args;
?>

<div class="three-col-wrap">
    <?php foreach ($args['columns'] as $idx => $arg) : ?>
        <?php $modifier = $idx == 2 ? 'col-last' : '' ?>

        <div class="col-4 <?php echo $modifier ?>">
            <div class="three-col-inner">

                <?php if( isset( $args['image']['url'] ) ) : ?>
                    <div class="three-col-image">
                        <img src="<?php echo $arg['image']['url'] ?>"/>
                    </div>
                <?php endif; ?>
                <div class="col_info">
                    <div class="three-col-title">
                        <h4><?php echo $arg['column_title'] ?></h4>
                    </div>
                    <div class="three-col-content">
                        <?php echo $arg['content'] ?>
                    </div>
                    <?php if ($arg['column_link']) : ?>
                        <div class="three-col-link">
                            <a href="<?php echo get_permalink($arg['column_link']->ID); ?>">Visit</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>


    <?php endforeach; ?>
</div>
