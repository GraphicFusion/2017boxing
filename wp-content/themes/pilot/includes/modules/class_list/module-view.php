<?php
	global $post;
	global $args;

	$qu = array( 'post_type' => 'wcs3_class', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' );
	$classes = new WP_Query( $qu );
?>
<?php if ( !empty($args['title']) ) : ?>
	<h3><?php echo $args['title']; ?></h3>
<?php endif; ?>

<div class="class-links">
	<div class="link-wrap">
		<a href="<?php echo get_site_url(); ?>/full-schedule">
			<span>
				All Classes
			</span>
		</a>
	</div>
	<?php
		while ( $classes->have_posts() ) : $classes->the_post();
	?>
	<div class="link-wrap">
		<a href="<?php echo get_permalink(); ?>">
			<span>  	
		  	<?php the_title(); ?>
			</span>
		</a>
	</div>
	
	<?php
		endwhile;
		wp_reset_postdata();
	?>
</div>