<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_competitors_layout () {
    $args = array(
		'title' => get_sub_field('competitors_block_title'),
    );
    return $args;
	}

?>