<?php 

	$qu = array( 'post_type' => 'competitors', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' );
	$competitors = new WP_Query( $qu );

	global $args;
?>
<div class="image-popup-wrap">

	<?php if ($args['title']) : ?>
		<h3><?php echo $args['title']; ?></h3>
	<?php endif; ?>

	<?php
		while ( $competitors->have_posts() ) : $competitors->the_post();
		$trainerid =  strtolower(str_replace(' ', '', get_the_title()));
	?>

		<div class="slide-wrapper" id="<?php echo $trainerid ?>">

			<div class="slide">
				<?php if (get_post_thumbnail_id() ):
					$image_url =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
				else:
					$image_url[0] = get_template_directory_uri().'/image/default-trainer.jpg';
				endif;
				?>
				<span><?php echo get_the_title(); ?></span>
				<div class="bg-image" style="background-image: url('<?php echo $image_url[0]; ?>'); background-position: center center;">
				</div>
			</div>
		</div>

		<div class="slide-content">
			<div class="left">
				<img class="trainer-image" src="<?php echo $image_url[0]; ?>" width="300" height="200"/>
			</div>
			<div class="right">
				<h3><?php echo get_the_title(); ?></h3>
				<p><?php echo the_content(); ?></p>
			</div>
		</div>
	<?php endwhile; ?>
</div>