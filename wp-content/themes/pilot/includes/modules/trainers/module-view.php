<?php 

	$qu = array( 'post_type' => 'wcs3_instructor', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' );
	$trainers = new WP_Query( $qu );

	global $args;
?>
<div class="image-popup-wrap">

	<?php if ($args['title']) : ?>
		<h3><?php echo $args['title']; ?></h3>
	<?php endif; ?>

	<?php
		while ( $trainers->have_posts() ) : $trainers->the_post();
		$trainerid =  strtolower(str_replace(' ', '', get_the_title()));
	?>

		<div class="slide-wrapper" id="<?php echo $trainerid ?>">

			<div class="slide">
				<?php if (get_post_thumbnail_id() ):
					$image_url =  wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
				else:
					$image_url[0] = get_template_directory_uri().'/image/default-trainer.jpg';
				endif;
				?>
				<span><?php echo get_the_title(); ?></span>
				<div class="bg-image" style="background-image: url('<?php echo $image_url[0]; ?>'); background-position: center center;">
				</div>
			</div>
		</div>

		<div class="slide-content">
			<div class="left">
				<img class="trainer-image" src="<?php echo $image_url[0]; ?>" width="300" height="200"/>
			</div>
			<div class="right">
				<h3><?php echo get_the_title(); ?></h3>
				<p><?php echo the_content(); ?></p>
				<?php $classes = wcs3_get_classes( 'list', 'all', 'all',  get_the_title());
					ksort($classes); // classes are returned as array where keys are days of week; ordered by pub date;
				if (!empty($classes)){ ?>
					<strong>Classes:</strong>
					<ul class="trainer-classes">
						<?php foreach ( $classes as $id => $type ) {
							foreach ( $type as $class ) {

								$class_title   = $class->class_title;
								$class_link    = sanitize_title( $class_title );
								$weekdays_dict = wcs3_get_weekdays();
								$day           = $weekdays_dict[ $class->weekday ];

								echo "<li><a href='/wcs3_class/" . $class_link . "'>";
								echo $class_title;
								echo "</a> on ";
								echo $day;
								echo " from ";
								echo $class->start_hour;
								echo " to ";
								echo $class->end_hour;
								echo " at <a class= 'trainer-location' href='/wcs3_location/";
								echo $class->location_slug;
								echo "'>";
								echo $class->location_title;
								echo "</a>";
								echo "</li>";

							}
						} ?>
					</ul>
				<?php }	?>
			</div>
		</div>
	<?php endwhile; ?>
</div>