<?php
/**
 * Template Name: Schedule
 */


get_template_part('templates/page', 'featured');
$query = new WP_Query( array(
			'post_type' => 'wcs3_location',
			'posts_per_page' => -1));
$count = 1;
?>

<div class="container pad-top">
		<div class="row">
			<div class="col-sm-12 content">
				<?php while ( have_posts() ) : the_post(); ?>
					<article <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php while ($query -> have_posts()): $query->the_post(); ?>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading<?php echo $count; ?>">
									    <h3 class="panel-title">
									        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $count; ?>" aria-expanded="false" aria-controls="collapse<?php echo $count; ?>">
									        	<?php echo get_the_title(); ?>
									        </a> 
									     </h3>
								    </div>
								    <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $count; ?>">
	      								<div class="panel-body">
										<?php $taxonomies = array('class-type'); ?>
										<?php $terms = get_terms($taxonomies); ?>
										<?php foreach ($terms as $t) { ?>
											<h4><?php echo $t->name; ?></h4>
											<?php $atts = array(
													    'layout' => 'normal',
													    'location' => get_the_title(),
														'class' => 'all',
														'instructor' => 'all',
														'weekday' => 'all',
													    'style' => 'normal',
													    'class_type' => $t->slug,
														'template' => null,
													); 
											?>
											<?php echo wcs3_standard_shortcode( $atts ); ?>
											<?php // wp_reset_query(); ?>
										<?php } ?>
										</div>
									</div>
								</div>
								<?php $count++; ?>
							<?php endwhile; ?>

<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading2">
<h3 class="panel-title"><a class="collapsed" href="http://www.crossfit8.com/#welcome" target="_blank">CROSSFIT 8 COUNT STUDIO </a></h3></div>
							</div>
						</div>
						<div class="footlinks">
							<?php wp_link_pages( array(
								'before' => '<nav class="page-nav"><p>' . __( 'Pages:', 'roots' ),
								'after'  => '</p></nav>'
							) ); ?>
						</div>
						<?php comments_template( '/templates/comments.php' ); ?>
					</article>
				<?php endwhile; ?>
			</div>
	</div>
</div>


<script type="text/javascript">
(function($) {

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function render_map( $el ) {

	// var
	var $markers = $el.find('.marker');

	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		// How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":10},{"lightness":30},{"gamma":0.5}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":-34}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#060a0a"},{"weight":1.4},{"lightness":14}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#232424"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#29221d"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffb624"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#966d1b"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#453b27"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#ffb92e"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}]

	};

	// create map
	var map = new google.maps.Map( $el[0], args);

	// add a markers reference
	map.markers = [];

	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});

	// center map
	center_map( map );

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var image = '/app/themes/boxing/assets/img/boxing.png';
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon		: image
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$(document).ready(function(){

	$('.acf-map').each(function(){

		render_map( $(this) );

	});

});

})(jQuery);
</script>
