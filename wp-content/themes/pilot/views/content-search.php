<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	
			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php pilot_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<?php if ( has_category(['videos', 'competitors']) ): ?>
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	<?php else: ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<a class="pilot-button" href="<?php echo esc_url( get_permalink() ); ?>">Read More</a>
		</div><!-- .entry-summary -->
	<?php endif; ?>

	<footer class="entry-footer">
		<?php pilot_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
