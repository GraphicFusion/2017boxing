<header class="site-header">
    <div class="container">
        <div class="site-branding">
            <h1><a href="<?php echo get_site_url(); ?>"><span class="screen-reader-text">Boxing Incorporated</span>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 32">
                <defs></defs>
                <title>Boxing Incorporated</title>
                <path d="M100.38,12,112,.82H99.54L89.75,9.09,80,.82H67.46L79.12,12l-12,11.36L0,32l73-3.18,16.78-14,16.78,14L193.63,32l-81.22-8.64ZM41.1,22.41l20-2.06a6.9,6.9,0,0,0,6.12-6.63V9.91a6.54,6.54,0,0,0-6.14-6.36l-20-1.16A5.68,5.68,0,0,0,35,8v9A5.39,5.39,0,0,0,41.1,22.41ZM44.86,10a2.05,2.05,0,0,1,2.23-2l8,.51a2.39,2.39,0,0,1,2.23,2.32v2.29a2.54,2.54,0,0,1-2.22,2.43l-8.06.9a1.94,1.94,0,0,1-2.22-1.93Zm79.08-6.6-9.76.48V19.73l9.76.85ZM26.06,24a6.9,6.9,0,0,0,6.12-6.63h0A4.76,4.76,0,0,0,29,13a4.76,4.76,0,0,0,3.23-4.32V7.87A6.54,6.54,0,0,0,26,1.51L24,1.4,0,0V26.64l23.25-2.39ZM18,18.84H9.32V16.11H22.86S23,18.84,18,18.84Zm-8.67-9V7.15H18c5,0,4.86,2.73,4.86,2.73ZM187.48.3l-19.63,1a6.47,6.47,0,0,0-6.15,6.3V17.86a6.77,6.77,0,0,0,6.13,6.53l3.8.33,22,1.91V11.14l-2.58,0-7.2-.05v7.67l-2.23-.15-.3,0-6-.4-1.63-.11a2.4,2.4,0,0,1-2.23-2.33V9.84a2.25,2.25,0,0,1,2.24-2.21l2.46,0,17.53-.23V0l-6.15.3ZM136.83,21.7a2.13,2.13,0,0,1,0-.26V10.59a2.33,2.33,0,0,1,2.24-2.27l7.63-.3a2.11,2.11,0,0,1,2.24,2.09V22.75l10,.87V7.71A5.74,5.74,0,0,0,152.76,2L150,2.15,133.59,3l-.71,0a6.47,6.47,0,0,0-6.15,6.3V20.82l6.13.53ZM223.17,7H220c-4.35,0-6.82,1.45-6.82,5.52V21.1H218V12.48c0-1,.79-1.41,2.07-1.41h3.13c1.28,0,2.07.4,2.07,1.41V21.1H230V12.48C230,8.41,227.52,7,223.17,7ZM205.24,21.1H210V7h-4.76ZM250,12.75v-.27C250,8.41,247.53,7,243.18,7H240c-4.35,0-6.82,1.45-6.82,5.52v3.09c0,4.07,2.47,5.52,6.82,5.52h3.13c4.35,0,6.82-1.45,6.82-5.52v-.27h-4.76v.27c0,1-.79,1.41-2.07,1.41H240c-1.28,0-2.07-.4-2.07-1.41V12.48c0-1,.79-1.41,2.07-1.41h3.13c1.28,0,2.07.4,2.07,1.41v.27Z"></path>
              </svg>
            </a></h1>
        </div><!-- .site-branding -->
        <nav class="main-navigation">
            <a class="screen-reader-text" href="#content"><?php _e( 'Skip to content', 'pilot' ); ?></a>
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
        </nav>
    </div>
</header>