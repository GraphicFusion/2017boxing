<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
    <?php get_all_blocks('header'); ?>

		<?php if ( has_post_thumbnail() ): ?>
			<div class="thumbnail-wrap"style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">
			</div>
		<?php endif; ?>

		<div class="title-wrap mfp-media">
			<?php if( is_front_page() ) : ?>
				<a href="https://www.youtube.com/watch?v=z2pYChv_T7Q" style="margin-bottom: 30px;" class="magnific pilot-video-button">Watch "I Love My Gym"</a>
			<?php endif; ?>
			<?php if( $pilot->use_default_page_titles && !get_field('hide_title') && !get_field('alternate_title') ) : ?>
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
			<?php endif; ?>
			<?php if( get_field('alternate_title') ) : ?>
				<h2 class="entry-title"><?php echo get_field('alternate_title'); ?></h2>
			<?php endif; ?>
		</div>
	</header>
	<div class="entry-content">
		<div class="layout-content">
			<?php the_content(); ?>
		</div>
	</div><!-- .entry-content -->
	<div class="modules">
		<?php 
			/*wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pilot' ),
				'after'  => '</div>',
			) );*/
			get_all_blocks('content'); // defined in /inc/content-blocks.php
		?>
	</div>

	<footer class="entry-footer">
		<?php get_all_blocks('footer-content'); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'pilot' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
</article>