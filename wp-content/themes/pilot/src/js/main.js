(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();

jQuery(document).ready(function($) {
    console.log( "Your JS is ready" );
});

jQuery(document).ready(function($) {
	// Initiate magnific

	$(".mfp-media a, .pilot-video-button").magnificPopup({
		type:'iframe',
		closeMarkup: '<button title="%title%" type="button" class="mfp-close"></button>'
	});

	$(".mfp-slide").magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}
	});

});

jQuery(window).on('load', function () {
	$ = jQuery;
	$('.wcs3-class-container').closest('.wcs3-cell').addClass('wcs3-filled');

	// Replace mobile icon for 2nd header design
	// hacky.  I'm sorry.
	if (
			$('body').hasClass('woocommerce-page') == true ||
			$('body').hasClass('single-wcs3_class') == true ||
			$('body').hasClass('page-template-template-class') == true ||
			$('body').hasClass('page-template-page-no-header-image') == true ||
			$('body').hasClass('single-wcs3_location') == true ||
			$('body').hasClass('blog') == true ||
			$('body').hasClass('archive') == true ||
			$('body').hasClass('single-post') == true ||
			$('body').hasClass('error404')
		) {
		var img = window.location.protocol + "//" + window.location.hostname + "/wp-content/themes/pilot/image/mob-icon-black.png";
		$('.responsive-menu-button-icon-active').attr('src', img);
	}

	// Find all iframes
	var $iframes = $( "iframe" );
	 
	// Find &#x26; save the aspect ratio for all iframes
	$iframes.each(function () {
	  $( this ).data( "ratio", this.height / this.width )
	    // Remove the hardcoded width &#x26; height attributes
	    .removeAttr( "width" )
	    .removeAttr( "height" );
	});
	 
	// Resize the iframes when the window is resized
	$( window ).resize( function () {
	  $iframes.each( function() {
	    // Get the parent container&#x27;s width
	    var width = $( this ).parent().width();
	    $( this ).width( width )
	      .height( width * $( this ).data( "ratio" ) );
	  });
	// Resize to fix all iframes on page load.
	}).resize();

});
