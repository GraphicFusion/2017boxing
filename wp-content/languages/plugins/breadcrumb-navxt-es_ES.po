# Translation of Stable (latest release) in Spanish (Spain)
# This file is distributed under the same license as the Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-03-19 00:33:59+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.1.0-alpha\n"
"Project-Id-Version: Stable (latest release)\n"

#: class.bcn_admin.php:419 class.bcn_network_admin.php:490
msgctxt "Paged as in when on an archive or post that is split into multiple pages"
msgid "Paged Breadcrumb"
msgstr "Migaja de Paginación"

#: class.bcn_admin.php:419 class.bcn_network_admin.php:490
msgid "Place the page number breadcrumb in the trail."
msgstr "Coloca el número de página en la Ruta de Navegación"

#: class.bcn_admin.php:420 class.bcn_network_admin.php:491
msgctxt "Paged as in when on an archive or post that is split into multiple pages"
msgid "Paged Template"
msgstr "Plantilla Para Paginación"

#: class.bcn_breadcrumb_trail.php:77
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Page %htitle%</span><meta property=\"position\" content=\"%position%\"></span>"
msgstr ""

#: class.bcn_breadcrumb_trail.php:103
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Search results for &#39;<a property=\"item\" typeof=\"WebPage\" title=\"Go to the first page of search results for %title%.\" href=\"%link%\" class=\"%type%\">%htitle%</a>&#39;</span><meta property=\"position\" content=\"%position%\"></span>"
msgstr ""

#: class.bcn_breadcrumb_trail.php:105
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Search results for &#39;%htitle%&#39;</span><meta property=\"position\" content=\"%position%\"></span>"
msgstr ""

#: class.bcn_breadcrumb_trail.php:118
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Articles by: <a title=\"Go to the first page of posts by %title%.\" href=\"%link%\" class=\"%type%\">%htitle%</a>"
msgstr ""

#: class.bcn_breadcrumb_trail.php:120
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Articles by: %htitle%</span><meta property=\"position\" content=\"%position%\"></span>"
msgstr ""

#: class.bcn_breadcrumb_trail.php:489
msgctxt "day archive breadcrumb date format"
msgid "d"
msgstr "d"

#: class.bcn_breadcrumb_trail.php:509
msgctxt "month archive breadcrumb date format"
msgid "F"
msgstr "m"

#: class.bcn_breadcrumb_trail.php:526
msgctxt "year archive breadcrumb date format"
msgid "Y"
msgstr "Y"

#: includes/class.mtekk_adminkit.php:312
msgid "Your settings are for an older version of this plugin and need to be migrated."
msgstr "Sus ajustes pertenecen  una versión más antigua de este plugin y necesita ser migrado."

#: includes/class.mtekk_adminkit.php:321
msgid "Your settings are for a newer version of this plugin."
msgstr "Sus ajustes pertenecen a una versión más moderna de este plugin."

#: includes/class.mtekk_adminkit.php:321
msgid "Attempt back migration now."
msgstr "Intentar regresar a la versión anterior ahora."

#: includes/class.mtekk_adminkit.php:337
msgid "One or more of your plugin settings are invalid."
msgstr "Uno o más de sus ajustes para este plugin son inválidos."

#: class.bcn_admin.php:263 class.bcn_network_admin.php:333
msgid "%sTranslate%s: Is your language not available? Visit the Breadcrumb NavXT translation project on WordPress.org to start translating."
msgstr "%sTraducir%s: ¿Tu idioma no está disponible? Visita la página de traducciones de Breadcrumb NavXT en WordPress.org para empezar a traducir."

#: class.bcn_breadcrumb.php:91
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"
msgstr "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Ir a %title%.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"

#: class.bcn_breadcrumb_trail.php:108
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% tag archives.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"
msgstr "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Ir al archivo de la etiqueta %title%.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"

#: class.bcn_breadcrumb_trail.php:113 class.bcn_breadcrumb_trail.php:129
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% archives.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"
msgstr "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Ir al archivo de %title%.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"

#: class.bcn_breadcrumb_trail.php:125
msgid "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% category archives.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"
msgstr "<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Ir al archivo de la categoría %title%.\" href=\"%link%\" class=\"%type%\"><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>"

#: class.bcn_breadcrumb_trail.php:391
msgid "$post global is not of type WP_Post"
msgstr "La global $post no corresponde al tipo de WP_Post"

#: class.bcn_network_admin.php:418 class.bcn_network_admin.php:436
msgid "Warning: Individual site settings will override any settings set in this page."
msgstr "Advertencia: los ajustes individuales del sitio modificarán cualquier ajuste en esta página."

#: class.bcn_network_admin.php:426 class.bcn_network_admin.php:430
msgid "Warning: Individual site settings may override any settings set in this page."
msgstr "Advertencia: los ajustes individuales del sitio pueden modificar los ajustes de esta página."

#: class.bcn_network_admin.php:63
msgid "Breadcrumb NavXT Network Settings"
msgstr "Ajustes de Red de Breadcrumb NavXT"

#: class.bcn_widget.php:32
msgid "Adds a breadcrumb trail to your sidebar"
msgstr "Agrega una ruta de navegación en su barra lateral"

#: class.bcn_widget.php:99
msgid "Title:"
msgstr "Título:"

#: class.bcn_widget.php:103
msgid "Text to show before the trail:"
msgstr "Texto a mostrar antes de las migas:"

#: class.bcn_widget.php:107
msgid "Output trail as:"
msgstr "Mostrar ruta como:"

#: class.bcn_widget.php:109
msgid "List"
msgstr "Lista"

#: class.bcn_widget.php:110
msgid "Google (RDFa) Breadcrumbs"
msgstr "Google (RDFa) Breadcrumbs"

#: class.bcn_widget.php:111
msgid "Plain"
msgstr "Texto plano"

#: class.bcn_widget.php:117
msgid "Link the breadcrumbs"
msgstr "Enlace a la Ruta"

#: class.bcn_widget.php:119
msgid "Reverse the order of the trail"
msgstr "Invertir el orden de la ruta"

#: class.bcn_widget.php:121
msgid "Hide the trail on the front page"
msgstr "Ocultar la ruta de la página principal"

#: includes/class.mtekk_adminkit.php:236
msgid "Settings"
msgstr "Configuraciones"

#: includes/class.mtekk_adminkit.php:312 includes/class.mtekk_adminkit.php:321
msgid "Migrate the settings now."
msgstr "Migrar los ajustes ahora."

#: includes/class.mtekk_adminkit.php:312
msgid "Migrate now."
msgstr "Migrar ahora."

#: includes/class.mtekk_adminkit.php:329
msgid "Your plugin install is incomplete."
msgstr "La instalación del plugin no se ha completado."

#: includes/class.mtekk_adminkit.php:329
msgid "Load default settings now."
msgstr "Cargar los ajustes predeterminados ahora."

#: includes/class.mtekk_adminkit.php:329
msgid "Complete now."
msgstr "Completar ahora."

#: includes/class.mtekk_adminkit.php:337
msgid "Attempt to fix settings now."
msgstr "Intentar arreglar los ajustes ahora."

#: includes/class.mtekk_adminkit.php:337
msgid "Fix now."
msgstr "Arreglar ahora."

#: includes/class.mtekk_adminkit.php:536
msgid "Settings successfully saved."
msgstr "Ajustes guardados exitosamente."

#: includes/class.mtekk_adminkit.php:536 includes/class.mtekk_adminkit.php:549
msgid "Undo the options save."
msgstr "Deshacer modificación de opciones."

#: includes/class.mtekk_adminkit.php:536 includes/class.mtekk_adminkit.php:549
#: includes/class.mtekk_adminkit.php:654 includes/class.mtekk_adminkit.php:678
#: includes/class.mtekk_adminkit.php:695
msgid "Undo"
msgstr "Deshacer"

#: includes/class.mtekk_adminkit.php:540
msgid "Settings did not change, nothing to save."
msgstr "Los ajustes no cambiaron, no hay nada que grabar."

#: includes/class.mtekk_adminkit.php:544
msgid "Settings were not saved."
msgstr "Los ajustes no fueron grabados."

#: includes/class.mtekk_adminkit.php:549
msgid "Some settings were not saved."
msgstr "Algunos ajustes no fueron grabados."

#: includes/class.mtekk_adminkit.php:550
msgid "The following settings were not saved:"
msgstr "Los siguientes ajustes no fueron grabados:"

#: includes/class.mtekk_adminkit.php:555
msgid "Please include this message in your %sbug report%s."
msgstr "Por favor, incluir este mensaje en su %sreporte de bugs%s."

#: includes/class.mtekk_adminkit.php:555
msgid "Go to the %s support post for your version."
msgstr "Ir a la documentación en línea de %s para su versión."

#: includes/class.mtekk_adminkit.php:654
msgid "Settings successfully imported from the uploaded file."
msgstr "Los ajustes de Breadcrumb NavXT se importaron exitosamente desde el archivo."

#: includes/class.mtekk_adminkit.php:654
msgid "Undo the options import."
msgstr "Deshacer importación de opciones."

#: includes/class.mtekk_adminkit.php:659
msgid "Importing settings from file failed."
msgstr "Falló la importación de los ajustes desde el archivo."

#: includes/class.mtekk_adminkit.php:678
msgid "Settings successfully reset to the default values."
msgstr "Los ajustes predeterminados de Breadcrumb NavXT se restauraron exitosamente."

#: includes/class.mtekk_adminkit.php:678
msgid "Undo the options reset."
msgstr "Deshacer reinicio de opciones."

#: includes/class.mtekk_adminkit.php:695
msgid "Settings successfully undid the last operation."
msgstr "Se deshizo exitosamente la operación anterior."

#: includes/class.mtekk_adminkit.php:695
msgid "Undo the last undo operation."
msgstr "Deshacer la última operación de \"deshacer\""

#: includes/class.mtekk_adminkit.php:730
msgid "Settings successfully migrated."
msgstr "Ajustes migrados exitosamente."

#: includes/class.mtekk_adminkit.php:737
msgid "Default settings successfully installed."
msgstr "Los ajustes predeterminados se instalaron exitosamente."

#: includes/class.mtekk_adminkit.php:833
msgid "Import settings from a XML file, export the current settings to a XML file, or reset to the default settings."
msgstr "Importar los ajustes de Breadcrumb NavXT desde un archivo XML, exportar los ajustes actuales a un archivo XML o reiniciar los valores predeterminados de Breadcrumb NavXT."

#: includes/class.mtekk_adminkit.php:836
msgid "Settings File"
msgstr "Archivo de Ajustes"

#: includes/class.mtekk_adminkit.php:839
msgid "Select a XML settings file to upload and import settings from."
msgstr "Seleccionar un archivo XML para cargar e importar los ajustes de este plugin."

#. Plugin Name of the plugin/theme
msgid "Breadcrumb NavXT"
msgstr "Breadcrumb NavXT"

#. Plugin URI of the plugin/theme
msgid "http://mtekk.us/code/breadcrumb-navxt/"
msgstr "http://mtekk.us/code/breadcrumb-navxt/"

#. Description of the plugin/theme
msgid "Adds a breadcrumb navigation showing the visitor&#39;s path to their current location. For details on how to use this plugin visit <a href=\"http://mtekk.us/code/breadcrumb-navxt/\">Breadcrumb NavXT</a>."
msgstr "Agrega un ruta de navegación mostrando al visitante su posición actual. Para detalles de cómo usar este plugin, visite la documentación oficial de <a href=\"http://mtekk.us/code/breadcrumb-navxt/\">Breadcrumb NavXT</a>."

#. Author of the plugin/theme
msgid "John Havlik"
msgstr "John Havlik | Traducción: Karin Sequén"

#. Author URI of the plugin/theme
msgid "http://mtekk.us/"
msgstr "http://mtekk.us/"

#: breadcrumb-navxt.php:35 class.bcn_admin.php:25
#: class.bcn_network_admin.php:25
msgid "Your PHP version is too old, please upgrade to a newer version. Your version is %1$s, Breadcrumb NavXT requires %2$s"
msgstr "Su versión de PHP es demasiado antigua, por favor, actualice a la nueva versión. Su versión es %1$s, el plugin Breadcrumb NavXT requiere la versión %2$s "

#: includes/class.mtekk_adminkit.php:113
msgid "Insufficient privileges to proceed."
msgstr "No tiene privilegios para proceder."

#: class.bcn_admin.php:256 class.bcn_network_admin.php:326
msgid "Tips for the settings are located below select options."
msgstr "Encontrará tips para los ajustes debajo de cada opción."

#: class.bcn_admin.php:257 class.bcn_network_admin.php:327
msgid "Resources"
msgstr "Recursos"

#: class.bcn_admin.php:258 class.bcn_network_admin.php:328
msgid "%sTutorials and How Tos%s: There are several guides, tutorials, and how tos available on the author's website."
msgstr "%sTutoriales e Instructivos%s: En el website del autor están disponibles varias guías, tutoriales e instructivos."

#: class.bcn_admin.php:258 class.bcn_network_admin.php:328
msgid "Go to the Breadcrumb NavXT tag archive."
msgstr "Ir al archivo de etiquetas de Breadcrumb NavXT"

#: class.bcn_admin.php:259 class.bcn_network_admin.php:329
msgid "%sOnline Documentation%s: Check out the documentation for more indepth technical information."
msgstr "%sDocumentación En Línea%s: Revise la documentación para más información técnica."

#: class.bcn_admin.php:259 class.bcn_network_admin.php:329
msgid "Go to the Breadcrumb NavXT online documentation"
msgstr "Ir a la documentación en línea de Breadcrumb NavXT (en inglés)"

#: class.bcn_admin.php:260 class.bcn_network_admin.php:330
msgid "%sReport a Bug%s: If you think you have found a bug, please include your WordPress version and details on how to reproduce the bug."
msgstr "%sReportar Errores%s: Si piensa que ha encontrado un error, por favor incluya su versión de WordPress y los detalles de cómo se reproduce el error."

#: class.bcn_admin.php:260 class.bcn_network_admin.php:330
msgid "Go to the Breadcrumb NavXT support post for your version."
msgstr "Ir a la documentación en línea de Breadcrumb NavXT para su versión."

#: class.bcn_admin.php:261 class.bcn_network_admin.php:331
msgid "Giving Back"
msgstr "Dar de vuelta"

#: class.bcn_admin.php:262 class.bcn_network_admin.php:332
msgid "%sDonate%s: Love Breadcrumb NavXT and want to help development? Consider buying the author a beer."
msgstr "%sDonar%s: ¿Le gusta Breadcrumb NavXT y desea colaborar con el desarrollo? Considere comprarle al autor una cerveza."

#: class.bcn_admin.php:262 class.bcn_network_admin.php:332
msgid "Go to PayPal to give a donation to Breadcrumb NavXT."
msgstr "Ir a la página de Paypal para darle un donativo a Breadcrumb NavXT."

#: class.bcn_admin.php:263 class.bcn_network_admin.php:333
msgid "Go to the Breadcrumb NavXT translation project."
msgstr "Ir al proyecto de traducción de Breadcrumb NavXT"

#: class.bcn_admin.php:268 class.bcn_admin.php:407 class.bcn_admin.php:408
#: class.bcn_network_admin.php:338 class.bcn_network_admin.php:478
#: class.bcn_network_admin.php:479
msgid "General"
msgstr "General"

#: class.bcn_admin.php:271 class.bcn_network_admin.php:341
msgid "For the settings on this page to take effect, you must either use the included Breadcrumb NavXT widget, or place either of the code sections below into your theme."
msgstr "Para que tomen efecto los ajustes en esta página, debe  incluir el widget del plugin o colocar el código en su plantilla."

#: class.bcn_admin.php:272 class.bcn_network_admin.php:342
msgid "Breadcrumb trail with separators"
msgstr "Ruta de Navegación con Separadores"

#: class.bcn_admin.php:278 class.bcn_network_admin.php:348
msgid "Breadcrumb trail in list form"
msgstr "Ruta de Navegación como Listado"

#: class.bcn_admin.php:287 class.bcn_network_admin.php:357
msgid "Quick Start"
msgstr "Inicio Rápido"

#: class.bcn_admin.php:290 class.bcn_network_admin.php:360
msgid "Using the code from the Quick Start section above, the following CSS can be used as base for styling your breadcrumb trail."
msgstr "Usando el código de Inicio Rápido de la sección anterior, el siguiente CSS puede utilizarse como base para darle estilo a su ruta de navegación."

#: class.bcn_admin.php:302 class.bcn_network_admin.php:372
msgid "Styling"
msgstr "Estilo"

#: class.bcn_admin.php:308 class.bcn_network_admin.php:378
msgid "Import/Export/Reset"
msgstr "Importar/Exportar/Reiniciar"

#: class.bcn_admin.php:332 class.bcn_network_admin.php:402
#: includes/class.mtekk_adminkit.php:841
msgid "Import"
msgstr "Importar"

#: class.bcn_admin.php:333 class.bcn_network_admin.php:403
#: includes/class.mtekk_adminkit.php:842
msgid "Export"
msgstr "Exportar"

#: class.bcn_admin.php:334 class.bcn_network_admin.php:404
#: includes/class.mtekk_adminkit.php:843
msgid "Reset"
msgstr "Reiniciar"

#: class.bcn_admin.php:352
msgid "Warning: Your network settings will override any settings set in this page."
msgstr "Advertencia: los ajustes de red modificarán cualquier ajuste en esta página."

#: class.bcn_admin.php:356 class.bcn_admin.php:360
msgid "Warning: Your network settings may override any settings set in this page."
msgstr "Advertencia: los ajustes de red pueden modificar los ajustes de esta página."

#: class.bcn_admin.php:365 class.bcn_network_admin.php:435
msgid "Warning: No BCN_SETTINGS_* define statement found, defaulting to BCN_SETTINGS_USE_LOCAL."
msgstr "Advertencia: No se definieron los ajustes BCN_SETTINGS_* (El ajuste predeterminado es BCN_SETTINGS_USE_LOCAL)"

#: class.bcn_admin.php:377 class.bcn_network_admin.php:448
msgid "Warning: Your are using a deprecated setting \"Title Length\" (see Miscellaneous &gt; Deprecated), please %1$suse CSS instead%2$s."
msgstr "Advertencia: Estás usando un ajuste descontinuado de \"Longitud de Títulos\" (ver Misceláneos &gt; Descontinuados), por favor %1$suse CSS en su lugar%2$s."

#: class.bcn_admin.php:377 class.bcn_admin.php:663
#: class.bcn_network_admin.php:448 class.bcn_network_admin.php:734
msgid "Go to the guide on trimming breadcrumb title lengths with CSS"
msgstr "Ir a la guía sobre cómo recortar la longitud de los títulos con CSS"

#: class.bcn_admin.php:64
msgid "Breadcrumb NavXT Settings"
msgstr "Opciones de Configuración de Breadcrumb NavXT"

#: class.bcn_admin.php:407 class.bcn_network_admin.php:478
msgid "A collection of settings most likely to be modified are located under this tab."
msgstr "Un conjunto de ajustes que probablemente serán modificados se encuentra en esta pestaña."

#: class.bcn_admin.php:411 class.bcn_network_admin.php:482
msgid "Breadcrumb Separator"
msgstr "Separador de Navegación"

#: class.bcn_admin.php:411 class.bcn_network_admin.php:482
msgid "Placed in between each breadcrumb."
msgstr "Colocado en medio de cada opción de navegación."

#: class.bcn_admin.php:415 class.bcn_network_admin.php:486
msgid "Current Item"
msgstr "Item Actual"

#: class.bcn_admin.php:418 class.bcn_network_admin.php:489
msgid "Link Current Item"
msgstr "Incluir Vínculo a Item Actual"

#: class.bcn_admin.php:418 class.bcn_network_admin.php:489
msgid "Yes"
msgstr "Sí"

#: class.bcn_admin.php:419 class.bcn_network_admin.php:490
msgid "Indicates that the user is on a page other than the first of a paginated archive or post."
msgstr ""

#: class.bcn_admin.php:420 class.bcn_network_admin.php:491
msgid "The template for paged breadcrumbs."
msgstr "Plantilla de ruta de navegación de entradas de varias páginas."

#: class.bcn_admin.php:424 class.bcn_admin.php:427
#: class.bcn_network_admin.php:495 class.bcn_network_admin.php:498
msgid "Home Breadcrumb"
msgstr "Incluir Inicio"

#: class.bcn_admin.php:427 class.bcn_network_admin.php:498
msgid "Place the home breadcrumb in the trail."
msgstr "Colocar un enlace a la página de inicio en la ruta de navegación."

#: class.bcn_admin.php:428 class.bcn_network_admin.php:499
msgid "Home Template"
msgstr "Plantilla de la Página de Inicio"

#: class.bcn_admin.php:428 class.bcn_network_admin.php:499
msgid "The template for the home breadcrumb."
msgstr "La plantilla de la ruta de navegación para la página de inicio."

#: class.bcn_admin.php:429 class.bcn_network_admin.php:500
msgid "Home Template (Unlinked)"
msgstr "Plantilla de la Página de Inicio (sin vínculo)"

#: class.bcn_admin.php:429 class.bcn_network_admin.php:500
msgid "The template for the home breadcrumb, used when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para la página de inicio, cuando el elemento de inicio no está enlazado."

#: class.bcn_admin.php:433 class.bcn_admin.php:436
#: class.bcn_network_admin.php:504 class.bcn_network_admin.php:507
msgid "Blog Breadcrumb"
msgstr "Incluir Inicio (del Blog)"

#: class.bcn_admin.php:436 class.bcn_network_admin.php:507
msgid "Place the blog breadcrumb in the trail."
msgstr "Colocar un enlace a la página de inicio en la ruta de navegación."

#: class.bcn_admin.php:437 class.bcn_network_admin.php:508
msgid "Blog Template"
msgstr "Plantilla del Blog"

#: class.bcn_admin.php:437 class.bcn_network_admin.php:508
msgid "The template for the blog breadcrumb, used only in static front page environments."
msgstr "La plantilla de la ruta de navegación para el blog, utilizado sólo cuando la página inicial es una página estática."

#: class.bcn_admin.php:438 class.bcn_network_admin.php:509
msgid "Blog Template (Unlinked)"
msgstr "Plantilla del Blog (sin vínculo)"

#: class.bcn_admin.php:438 class.bcn_network_admin.php:509
msgid "The template for the blog breadcrumb, used only in static front page environments and when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para el blog, utilizado sólo cuando la página inicial es una página estática y el elemento de inicio no está enlazado."

#: class.bcn_admin.php:442 class.bcn_network_admin.php:513
msgid "Mainsite Breadcrumb"
msgstr "Migas (Breadcrumbs) del Sitio Principal"

#: class.bcn_admin.php:445 class.bcn_network_admin.php:516
msgid "Main Site Breadcrumb"
msgstr "Ruta del Sitio Principal"

#: class.bcn_admin.php:445 class.bcn_network_admin.php:516
msgid "Place the main site home breadcrumb in the trail in an multisite setup."
msgstr "Colocar un enlace a la página de inicio del sitio principal en la ruta de navegación de una configuración con múltiples blogs."

#: class.bcn_admin.php:446 class.bcn_network_admin.php:517
msgid "Main Site Home Template"
msgstr "Plantilla de Inicio del Sitio Principal"

#: class.bcn_admin.php:446 class.bcn_network_admin.php:517
msgid "The template for the main site home breadcrumb, used only in multisite environments."
msgstr "La plantilla de enlace para la página de inicio del sitio principal, utilizada sólo en configuraciones con múltiples blogs."

#: class.bcn_admin.php:447 class.bcn_network_admin.php:518
msgid "Main Site Home Template (Unlinked)"
msgstr "Plantilla de Inicio del Sitio Principal (sin vínculo)"

#: class.bcn_admin.php:447 class.bcn_network_admin.php:518
msgid "The template for the main site home breadcrumb, used only in multisite environments and when the breadcrumb is not linked."
msgstr "La plantilla de enlace para la página de inicio del sitio principal, utilizada sólo en configuraciones con múltiples blogs y el elemento de navegación no está enlazado."

#: class.bcn_admin.php:454 class.bcn_network_admin.php:525
msgid "The settings for all post types (Posts, Pages, and Custom Post Types) are located under this tab."
msgstr "Los ajustes para todos los tipos de entradas (Entradas, Páginas, Personalizadas) se encuentran en esta pestaña."

#: class.bcn_admin.php:454 class.bcn_network_admin.php:525
msgid "Post Types"
msgstr "Tipos de Entradas"

#: class.bcn_admin.php:455 class.bcn_network_admin.php:526
msgid "Posts"
msgstr "Entradas"

#: class.bcn_admin.php:458 class.bcn_network_admin.php:529
msgid "Post Template"
msgstr "Plantilla de Entradas"

#: class.bcn_admin.php:458 class.bcn_network_admin.php:529
msgid "The template for post breadcrumbs."
msgstr "La plantilla de la ruta de navegación para entradas."

#: class.bcn_admin.php:459 class.bcn_network_admin.php:530
msgid "Post Template (Unlinked)"
msgstr "Plantilla de la Entrada (sin vínculo)"

#: class.bcn_admin.php:459 class.bcn_network_admin.php:530
msgid "The template for post breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para entradas, cuando el elemento no está enlazado."

#: class.bcn_admin.php:460 class.bcn_network_admin.php:531
msgid "Post Hierarchy Display"
msgstr "Jerarquía para desplegar entradas"

#: class.bcn_admin.php:460 class.bcn_network_admin.php:531
msgid "Show the hierarchy (specified below) leading to a post in the breadcrumb trail."
msgstr "Mostrar la jerarquía (especificada abajo) que lleva a la entrada en la ruta de navegación."

#: class.bcn_admin.php:464 class.bcn_network_admin.php:535
msgid "Post Hierarchy"
msgstr "Jerarquía de entradas"

#: class.bcn_admin.php:468 class.bcn_admin.php:586
#: class.bcn_network_admin.php:539 class.bcn_network_admin.php:657
msgid "Categories"
msgstr "Categorías"

#: class.bcn_admin.php:469 class.bcn_admin.php:547
#: class.bcn_network_admin.php:540 class.bcn_network_admin.php:618
msgid "Dates"
msgstr "Fechas"

#: class.bcn_admin.php:470 class.bcn_admin.php:593
#: class.bcn_network_admin.php:541 class.bcn_network_admin.php:664
msgid "Tags"
msgstr "Etiquetas"

#: class.bcn_admin.php:472 class.bcn_admin.php:546
#: class.bcn_network_admin.php:543 class.bcn_network_admin.php:617
msgid "Post Parent"
msgstr "Entrada o Página superior"

#: class.bcn_admin.php:488 class.bcn_admin.php:571
#: class.bcn_network_admin.php:559 class.bcn_network_admin.php:642
msgid "The hierarchy which the breadcrumb trail will show. Note that the \"Post Parent\" option may require an additional plugin to behave as expected since this is a non-hierarchical post type."
msgstr "La jerarquia en la que se muestra la ruta de migas. Tome nota de que la opción de \"Entrada/Página Superior\" puede requerir un plugin adicional para comportarse como se espera, ya que este es un tipo de publicación sin jerarquía."

#: class.bcn_admin.php:492 class.bcn_network_admin.php:563
msgid "Pages"
msgstr "Páginas"

#: class.bcn_admin.php:495 class.bcn_network_admin.php:566
msgid "Page Template"
msgstr "Plantilla de la Página"

#: class.bcn_admin.php:495 class.bcn_network_admin.php:566
msgid "The template for page breadcrumbs."
msgstr "La plantilla de la ruta de navegación para páginas."

#: class.bcn_admin.php:496 class.bcn_network_admin.php:567
msgid "Page Template (Unlinked)"
msgstr "Plantilla de la Página (sin vínculo)"

#: class.bcn_admin.php:496 class.bcn_network_admin.php:567
msgid "The template for page breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para páginas, cuando el elemento no está enlazado."

#: class.bcn_admin.php:499 class.bcn_network_admin.php:570
msgid "Attachments"
msgstr "Adjuntos"

#: class.bcn_admin.php:502 class.bcn_network_admin.php:573
msgid "Attachment Template"
msgstr "Plantilla de Archivo Adjunto"

#: class.bcn_admin.php:502 class.bcn_network_admin.php:573
msgid "The template for attachment breadcrumbs."
msgstr "La plantilla de la ruta de navegación para archivos adjuntos."

#: class.bcn_admin.php:503 class.bcn_network_admin.php:574
msgid "Attachment Template (Unlinked)"
msgstr "Plantilla del Archivo Adjunto (sin vínculo)"

#: class.bcn_admin.php:503 class.bcn_network_admin.php:574
msgid "The template for attachment breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para archivos adjuntos, cuando el elemento no está enlazado."

#: class.bcn_admin.php:523 class.bcn_admin.php:624
#: class.bcn_network_admin.php:594 class.bcn_network_admin.php:695
msgid "%s Template"
msgstr "Plantilla de %s"

#: class.bcn_admin.php:523 class.bcn_admin.php:624
#: class.bcn_network_admin.php:594 class.bcn_network_admin.php:695
msgid "The template for %s breadcrumbs."
msgstr "La plantilla de la ruta de navegación para %s."

#: class.bcn_admin.php:524 class.bcn_admin.php:625
#: class.bcn_network_admin.php:595 class.bcn_network_admin.php:696
msgid "%s Template (Unlinked)"
msgstr "Plantilla de %s (sin vínculo)"

#: class.bcn_admin.php:524 class.bcn_admin.php:625
#: class.bcn_network_admin.php:595 class.bcn_network_admin.php:696
msgid "The template for %s breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para %s, cuando el elemento no está enlazado."

#: class.bcn_admin.php:529 class.bcn_network_admin.php:600
msgid "%s Root Page"
msgstr "%s Página Raíz"

#: class.bcn_admin.php:532 class.bcn_network_admin.php:603
msgid "&mdash; Select &mdash;"
msgstr "&mdash; Seleccione &mdash;"

#: class.bcn_admin.php:536 class.bcn_network_admin.php:607
msgid "%s Archive Display"
msgstr "Despliegue de Archivos de %s"

#: class.bcn_admin.php:536 class.bcn_network_admin.php:607
msgid "Show the breadcrumb for the %s post type archives in the breadcrumb trail."
msgstr "Mostrar la ruta de navegación para los archivos de las entradas de tipo "

#: class.bcn_admin.php:537 class.bcn_network_admin.php:608
msgid "%s Hierarchy Display"
msgstr "%s Jerarquía de Despliegue"

#: class.bcn_admin.php:537 class.bcn_network_admin.php:608
msgid "Show the hierarchy (specified below) leading to a %s in the breadcrumb trail."
msgstr "Mostrar la jerarquía (especificada abajo) que lleva a %s  en la ruta de navegación."

#: class.bcn_admin.php:541 class.bcn_network_admin.php:612
msgid "%s Hierarchy"
msgstr "%s Jerarquía"

#: class.bcn_admin.php:567 class.bcn_network_admin.php:638
msgid "The hierarchy which the breadcrumb trail will show."
msgstr "La jerarquía que mostrará la ruta de migas."

#: class.bcn_admin.php:585 class.bcn_network_admin.php:656
msgid "The settings for all taxonomies (including Categories, Tags, and custom taxonomies) are located under this tab."
msgstr "Los ajustes para todas las taxonomías (incluyendo Categorías, Etiquetas y Personalizadas) están en esta pestaña."

#: class.bcn_admin.php:585 class.bcn_network_admin.php:656
msgid "Taxonomies"
msgstr "Taxonomías"

#: class.bcn_admin.php:589 class.bcn_network_admin.php:660
msgid "Category Template"
msgstr "Plantilla de Categoría"

#: class.bcn_admin.php:589 class.bcn_network_admin.php:660
msgid "The template for category breadcrumbs."
msgstr "La plantilla de la ruta de navegación para categorías."

#: class.bcn_admin.php:590 class.bcn_network_admin.php:661
msgid "Category Template (Unlinked)"
msgstr "Plantilla de la Categoría (sin vínculo)"

#: class.bcn_admin.php:590 class.bcn_network_admin.php:661
msgid "The template for category breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para categorías, cuando el elemento no está enlazado."

#: class.bcn_admin.php:596 class.bcn_network_admin.php:667
msgid "Tag Template"
msgstr "Plantilla de la Etiqueta"

#: class.bcn_admin.php:596 class.bcn_network_admin.php:667
msgid "The template for tag breadcrumbs."
msgstr "La plantilla de la ruta de navegación para etiquetas."

#: class.bcn_admin.php:597 class.bcn_network_admin.php:668
msgid "Tag Template (Unlinked)"
msgstr "Plantilla de la Etiqueta (sin vínculo)"

#: class.bcn_admin.php:597 class.bcn_network_admin.php:668
msgid "The template for tag breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para etiquetas, cuando el elemento no está enlazado."

#: class.bcn_admin.php:600 class.bcn_network_admin.php:671
msgid "Post Formats"
msgstr "Formatos de Entradas"

#: class.bcn_admin.php:603 class.bcn_network_admin.php:674
msgid "Post Format Template"
msgstr "Plantilla para Formato de la Entrada"

#: class.bcn_admin.php:603 class.bcn_network_admin.php:674
msgid "The template for post format breadcrumbs."
msgstr "La plantilla para el formato de la ruta de migas en la entrada"

#: class.bcn_admin.php:604 class.bcn_network_admin.php:675
msgid "Post Format Template (Unlinked)"
msgstr "Plantilla para Formato de la Entrada (sin enlace)"

#: class.bcn_admin.php:604 class.bcn_network_admin.php:675
msgid "The template for post_format breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla para el formato de la ruta de migas en la entrada, utilizado sólo cuando la miga no tiene enlace."

#: class.bcn_admin.php:634 class.bcn_network_admin.php:705
msgid "The settings for author and date archives, searches, and 404 pages are located under this tab."
msgstr "Los ajustes para autor y archivos, búsquedas, y páginas 404 están en esta pestaña."

#: class.bcn_admin.php:634 class.bcn_admin.php:643
#: class.bcn_network_admin.php:705 class.bcn_network_admin.php:714
msgid "Miscellaneous"
msgstr "Misceláneos"

#: class.bcn_admin.php:635 class.bcn_network_admin.php:706
msgid "Author Archives"
msgstr "Archivos de Autor"

#: class.bcn_admin.php:638 class.bcn_network_admin.php:709
msgid "Author Template"
msgstr "Plantilla de Autor"

#: class.bcn_admin.php:638 class.bcn_network_admin.php:709
msgid "The template for author breadcrumbs."
msgstr "La plantilla de la ruta de navegación para autores."

#: class.bcn_admin.php:639 class.bcn_network_admin.php:710
msgid "Author Template (Unlinked)"
msgstr "Plantilla del Autor (sin vínculo)"

#: class.bcn_admin.php:639 class.bcn_network_admin.php:710
msgid "The template for author breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para autores, cuando el elemento no está enlazado."

#: class.bcn_admin.php:640 class.bcn_network_admin.php:711
msgid "Author Display Format"
msgstr "Formato de Despliegue de Autor"

#: class.bcn_admin.php:640 class.bcn_network_admin.php:711
msgid "display_name uses the name specified in \"Display name publicly as\" under the user profile the others correspond to options in the user profile."
msgstr "display_name utiliza el nombre especificado en \"Mostrar este nombre públicamente\" en el perfil del usuario, la otras opciones corresponden a los campos nombre (first_name), apellido (last_name) y alias (nickname) en el perfil del usuario."

#: class.bcn_admin.php:646 class.bcn_network_admin.php:717
msgid "Date Template"
msgstr "Plantilla de la Fecha"

#: class.bcn_admin.php:646 class.bcn_network_admin.php:717
msgid "The template for date breadcrumbs."
msgstr "La plantilla de la ruta de navegación para fechas."

#: class.bcn_admin.php:647 class.bcn_network_admin.php:718
msgid "Date Template (Unlinked)"
msgstr "Plantilla de la Fecha (sin vínculo)"

#: class.bcn_admin.php:647 class.bcn_network_admin.php:718
msgid "The template for date breadcrumbs, used only when the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para fechas, cuando el elemento no está enlazado."

#: class.bcn_admin.php:648 class.bcn_network_admin.php:719
msgid "Search Template"
msgstr "Plantilla de la Búsqueda"

#: class.bcn_admin.php:648 class.bcn_network_admin.php:719
msgid "The anchor template for search breadcrumbs, used only when the search results span several pages."
msgstr "Plantilla del vínculo a la primera página de resultados de búsqueda en la ruta de navegación, utilizado sólo cuando éstos ocupan más de una página."

#: class.bcn_admin.php:649 class.bcn_network_admin.php:720
msgid "Search Template (Unlinked)"
msgstr "Plantilla de la Búsqueda (sin vínculo)"

#: class.bcn_admin.php:649 class.bcn_network_admin.php:720
msgid "The anchor template for search breadcrumbs, used only when the search results span several pages and the breadcrumb is not linked."
msgstr "La plantilla de la ruta de navegación para la primera página de resultados de búsqueda, utilizado sólo cuando éstos ocupan más de una página y el elemento no está enlazado."

#: class.bcn_admin.php:650 class.bcn_network_admin.php:721
msgid "404 Title"
msgstr "Título Error 404"

#: class.bcn_admin.php:651 class.bcn_network_admin.php:722
msgid "404 Template"
msgstr "Plantilla de Error 404"

#: class.bcn_admin.php:651 class.bcn_network_admin.php:722
msgid "The template for 404 breadcrumbs."
msgstr "La plantilla para la ruta de la página de error 404."

#: class.bcn_admin.php:654 class.bcn_network_admin.php:725
msgid "Deprecated"
msgstr "Descontinuado"

#: class.bcn_admin.php:658 class.bcn_network_admin.php:729
msgid "Title Length"
msgstr "Longitud del título"

#: class.bcn_admin.php:663 class.bcn_network_admin.php:734
msgid "Limit the length of the breadcrumb title. (Deprecated, %suse CSS instead%s)"
msgstr "Limita la longitud de los títulos de la ruta (Descontinuado, %suse CSS en su lugar%s)"

#: class.bcn_admin.php:668 class.bcn_network_admin.php:739
msgid "Max Title Length: "
msgstr "Longitud Máxima del Título:"

#: class.bcn_admin.php:680 class.bcn_network_admin.php:751
msgid "Save Changes"
msgstr "Grabar Cambios"

#: class.bcn_breadcrumb_trail.php:100
msgid "404"
msgstr "Página no encontrada"
