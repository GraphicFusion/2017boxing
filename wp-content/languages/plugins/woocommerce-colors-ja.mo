��          �      L      �     �     �     �               %     1     A  F   T     �  	   �  *   �     �     �  0   
     ;  >   Y  8   �  '  �     �          .     A     R     n     z     �  D   �     �  	   �  I         J     ]  4   r  -   �  `   �  H   6                                                                                
         	           Content Background Color %s Highlight Color %s Primary Color %s Secondary Color %s Subtext Color %s WooCommerce WooCommerce 2.3 WooCommerce Colors WooCommerce Colors depends on the last version of %s or later to work! WooCommerce Colors. WooThemes action buttons/price slider/layered nav UI buttons and tabs http://woothemes.com http://wordpress.org/plugins/woocommerce-colors/ price labels and sale flashes used for certain text and asides - breadcrumbs, small text etc your themes page background - used for tab active states PO-Revision-Date: 2016-10-25 00:53:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/2.3.0-alpha
Language: ja_JP
Project-Id-Version: Plugins - WooCommerce Colors - Stable (latest release)
 コンテンツの背景色 %s ハイライト色 %s メインの色 %s 2番目の色 %s サブテキストの色 %s WooCommerce WooCommerce 2.3 WooCommerce Colors WooCommerce Colors は動作するのに %s 以降が必要です。 WooCommerce Colors. WooThemes アクションボタン / 価格スライダー / 階層状の UI ナビ ボタンとタブ http://woothemes.com https://ja.wordpress.org/plugins/woocommerce-colors/ 販売ラベルとセールのフラッシュ 特定のテキストやアサイドに使用 - パンくずリスト、小さなテキスト等 テーマのページ背景 - タブがアクティブの状態に使用 